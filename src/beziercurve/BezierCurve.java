package beziercurve;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.FPSAnimator;
import javax.swing.JFrame;

public class BezierCurve {

    public static void main(String[] args) {
        // TODO code application logic here
        
        //Con las teclas arriba y abajo controla el grozor de la linea
        //Con las teclas derecha e izquierda controla el numero de iteraciones (suavidad de la curva)
        
        GLProfile perfil = GLProfile.getDefault();
        GLCapabilities capacidades = new GLCapabilities(perfil);
        PanelCanvas panelCanvas = new PanelCanvas(capacidades);
        
        GraphicListener listener = new GraphicListener();
        FPSAnimator animador = new FPSAnimator(panelCanvas, 24, true);
        
        panelCanvas.addGLEventListener(listener);
        
        JFrame frame = new JFrame("Curve Bezier Miranda");
        
        frame.addKeyListener(listener);
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(panelCanvas);
        frame.setVisible(true);
        animador.start();
           
    }
    
}
