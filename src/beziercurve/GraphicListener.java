package beziercurve;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.nio.FloatBuffer;

class GraphicListener extends KeyAdapter implements GLEventListener {
    
    float[][] puntosControl;
    private FloatBuffer puntosControlBuf;
    GL2 gl2;
    float grozor = 5;
    float t = 10;

    @Override
    public void init(GLAutoDrawable drawable) {
        
        System.out.println("Iniciando");

        gl2 = drawable.getGL().getGL2();
        
        puntosControl = new float[][]{
            { -4.0f, -4.0f, 0.0f },
            { -2.0f, 3.0f, 0.0f },
            { 2.0f, -4.0f, 0.0f },
            { 4.0f, 3.0f, 0.0f } };
        
        puntosControlBuf = FloatBuffer.allocate(puntosControl[0].length * puntosControl.length);
        
        for (float[] puntosControl1 : puntosControl) {
            for (int j = 0; j < 3; j++) {
                puntosControlBuf.put(puntosControl1[j]);
            }
        }
        puntosControlBuf.rewind();
        
        gl2.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        gl2.glShadeModel(GL2.GL_FLAT);
        gl2.glMap1f(GL2.GL_MAP1_VERTEX_3, 0.0f, 1.0f, 3, 4, puntosControlBuf);
        gl2.glEnable(GL2.GL_MAP1_VERTEX_3);        
        
    }

    @Override
    public void dispose(GLAutoDrawable drawable) {
        System.out.println("dispose");
        
    }

    @Override
    public void display(GLAutoDrawable drawable) {
        System.out.println("Dibijando");
        
        gl2 = drawable.getGL().getGL2();
 
        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT);
        gl2.glColor3f(1.0f, 0.0f, 0.0f);
        
        gl2.glLineWidth(grozor);
        gl2.glBegin(GL2.GL_LINE_STRIP);
        
            for (int i = 0; i <= t; i++){
              gl2.glEvalCoord1f((float) i / (float) t);
            }
            
        gl2.glEnd();
        
        /* Muestra los puntos de control como puntos. */
        gl2.glPointSize(10.0f);
        gl2.glColor3f(1.0f, 1.0f, 0.0f);
        
        gl2.glBegin(GL2.GL_POINTS);
        
            for (int i = 0; i < 4; i++){
              gl2.glVertex3fv(puntosControlBuf);
              puntosControlBuf.position(i *3);
            }
        
        gl2.glEnd();
        
        gl2.glFlush();

    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        System.out.println("Cambiendo de tamaño");
        
        gl2 = drawable.getGL().getGL2();
 
        gl2.glViewport(0, 0, width, height);
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        
        if (width <= height)
            gl2.glOrtho(-5.0, 5.0, -5.0 * (float) height / (float) width, 5.0 * (float) height / (float) width, -5.0, 5.0);
        else 
            gl2.glOrtho(-5.0 * (float) width / (float) height, 5.0 * (float) width / (float) height, -5.0, 5.0, -5.0, 5.0);
        
        gl2.glMatrixMode(GL2.GL_MODELVIEW);
        gl2.glLoadIdentity();
    }
    
    @Override
    public void keyPressed(KeyEvent e){
        
        switch(e.getKeyCode()){
            case KeyEvent.VK_UP:
                System.out.println("derecha");
                grozor += 1;
                break;
            case KeyEvent.VK_DOWN:
                System.out.println("abajo");
                grozor -= 1;
                break;
                
            case KeyEvent.VK_LEFT:
                t -= 1;
                break;
                
            case KeyEvent.VK_RIGHT:
                t += 1;
                break;     
                
        }
        
    }

}
